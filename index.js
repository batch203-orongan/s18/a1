console.log("Hello, World!");

function addTwoNumbers(num1, num2){
	console.log("Displayed sum of "+num1+ " and "+num2 );
	console.log(num1+num2);
}

function diffTwoNumbers(num1, num2){
	console.log("Displayed difference of "+num1+ " and "+num2 );
	console.log(num1-num2);
}

addTwoNumbers(5,15);
diffTwoNumbers(20,5);

function multiplyTwoNumbers(num1, num2){
	console.log("The product of "+num1+ " and "+num2 );
	return num1*num2;
}

function quotientTwoNumbers(num1, num2){
	console.log("The quotient of "+num1+ " and "+num2 );
	return num1/num2;
}

let product = multiplyTwoNumbers(50,10);
console.log(product);

let quotient = quotientTwoNumbers(50,10);
console.log(quotient);

function areaCircle(radius){
	const pi = 3.1416
	console.log("The result of getting the area of a circle with 15 radius: ");	
	return pi*radius**2
}

let circleArea = areaCircle(15);
console.log(circleArea);

function averageFourNum(num1, num2, num3, num4){
	console.log("The average of "+num1+ ", "+num2+", "+num3+", "+num4+ ":");
	return (num1+num2+num3+num4)/4;
}

let averageVar = averageFourNum(20,40,60,80);
console.log(averageVar);

function passedChecking(yourScore,totalScore){
	let score = (yourScore/totalScore)*100;
	let isPassed = ((score>75)||(score=75));
	console.log("Is "+yourScore + "/"+ totalScore+ " a passing score?");
	return isPassed;
}

let isPassingScore = passedChecking(38,50);
console.log(isPassingScore);